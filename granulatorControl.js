var common = require('common');
var log = common.log;
var assert = common.assert;

var path = "live_set tracks 2 devices 1";
var liveObject = new LiveAPI(path);
//log(liveObject.info);

var numParams = liveObject.getcount("parameters");
log(numParams);

for (n = 1; n < 3; n++) {
    var paramPath = path + " parameters " + n;
    var param = new LiveAPI(paramPath);
    log(n, ": ", param.get("value"));
}

/*
log(param.info);
log(param.get("value"));
param.set("value", "12.0");
log(param.get("value"));
*/
