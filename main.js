/*
* Thanks to https://docs.cycling74.com/max7/vignettes/jsrequire for require usage.
*/

var common = require('common');
var log = common.log;
var assert = common.assert;

var chordDeviceReq = require('chordDevice');
var ChordDevice = chordDeviceReq.ChordDevice;
var chord1 = new ChordDevice("live_set tracks 0 devices 1"); 
var chord2 = new ChordDevice("live_set tracks 1 devices 1"); 
var chord3 = new ChordDevice("live_set tracks 2 devices 0");  

var arpegDeviceReq = require('arpegDevice');
var ArpegDevice = arpegDeviceReq.ArpegDevice;
var arpeg1 = new ArpegDevice("live_set tracks 0 devices 2");
var arpeg2 = new ArpegDevice("live_set tracks 1 devices 2");

var clipReq = require('clip');
var Clip = clipReq.Clip;
var Note = clipReq.Note;

var clip1 = new Clip("live_set tracks 0 clip_slots 0 clip");
var clip2 = new Clip("live_set tracks 1 clip_slots 0 clip");

//var granControl = new LiveAPI("live_set tracks 2 devices 1");
var granPos = new LiveAPI("live_set tracks 2 devices 1 parameters 1");
var granSize = new LiveAPI("live_set tracks 2 devices 1 parameters 2");
var granSpray = new LiveAPI("live_set tracks 2 devices 1 parameters 3");

function addSimpleNotesToClips() {
    var notes = [];
    notes.push(new Note(48, "0.0", "8.0", 90, 0));
    notes.push(new Note(55, "2.0", "4.0", 80, 0));
    notes.push(new Note(56, "4.0", "4.0", 90, 0));
    notes.push(new Note(51, "6.0", "2.0", 80, 0));
    clip1.setNotes(notes);
    notes = [];
    notes.push(new Note(60, "0.0", "8.0", 100, 0));
    notes.push(new Note(67, "2.0", "4.0", 80, 0));
    notes.push(new Note(68, "4.0", "4.0", 100, 0));
    notes.push(new Note(63, "6.0", "2.0", 80, 0));
    clip2.setNotes(notes);
}

function choosePreset(preset, track) {
    log("Preset: ", preset);
    var chordDevice = chord1;
    var arpegDevice = arpeg1;
    if (track == 2) {
        chordDevice = chord2;
        arpegDevice = arpeg2;
    }
    if (preset == 0) {
        chordDevice.pitchKnob(1, "7.00", "0.95");
        chordDevice.pitchKnob(2, "5.00", "0.65");
        chordDevice.pitchKnob(3, "0.00", "0.45");
        chordDevice.pitchKnob(4, "3.00", "0.55");
        arpegDevice.rateKnob("12.0");
        arpegDevice.gateKnob("120.0");
        arpegDevice.stepsKnob("0.0");
        chord3.pitchKnob(1, "0.00", "1.00");
        chord3.pitchKnob(2, "0.00", "1.00");
        granPos.set("value", "0");
        granSize.set("value", "40");
        granSpray.set("value", "40");
        return;
    } 

    if (preset >= 16) {
        clip1.stop();
        clip2.stop();
        log("Stopping clips");
        return;
    }

    granPos.set("value", (Math.random() * 40 + 20).toString());
    if (chord3.pitchKnob(2) != 0) {
        granSize.set("value", (Math.random() * 50 + 20).toString());
    } else {
        granSize.set("value", (Math.random() * 20 + 20).toString());
    }
    granSpray.set("value", (Math.random() * 100).toString());

    if (track == 2) {
        if (preset % 8 == 0) {
            arpegDevice.rateKnob("8.0");
            arpegDevice.gateKnob("80.0");
            arpegDevice.stepsKnob("1.0");
            clip2.fire();
            clip1.stop();
            chord3.pitchKnob(2, "12.00", "0.01");
            granSize.set("value", (Math.random() * 20 + 20).toString());
            return;
        }
        
        if (preset % 6 == 0) {
            arpegDevice.rateKnob("10.0");
            arpegDevice.gateKnob("100.0");
            arpegDevice.stepsKnob("1.0");
            clip2.stop();
        } else if (preset % 6 == 2) {
            clip2.fire();
        } else if (preset % 6 == 3) {
            clip2.stop();
        } else if (preset % 6 == 4) {
            clip2.fire();
        }
    }

    if (preset % 4 == 3) {
        if (track == 1) {
            //chordDevice.pitchKnob(3, "-12.00", "0.45");
            arpegDevice.rateKnob("12.0");
            arpegDevice.gateKnob("120.0");
            arpegDevice.stepsKnob("1.0");
        }
    } else if (preset % 4 == 1) {
        clip1.stop();
        chord3.pitchKnob(1, "-12.00", "0.85");
    } else if (preset % 4 == 2) {
        clip1.fire();
        chord3.pitchKnob(1, "-12.00", "0.40");
        chord3.pitchKnob(2, "0.00", "1.00");
    } else if (clip1.get("is_playing") == 0) {
        clip1.fire();
    }
}

choosePreset(0, 1);
choosePreset(0, 2);
addSimpleNotesToClips();

function anything() {
    var input = arrayfromargs(messagename, arguments);
    post(input, '\n');
    log("Received: ", input, "length: ", input.length);
    assert (input.length == 2, "Check input formatting.");
    if (input[0] == "preset") {
        var preset = parseInt(input[1]);
        if (preset == 0) {
            clip1.fire();
            return;
        }
        var track = 1;
        if (preset >= 4) track = 2;
        choosePreset(parseInt(input[1]), track);
    } else {
        log(input[0], "<- input[0]");
        assert (false);
    }
}
