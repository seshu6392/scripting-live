var common = require('common');
var assert = common.assert;

/*
* Note: Must first create a midi clip at target slot before doing anything.
*/
function Clip(path) {
	if (!path) path = "live_set view highlighted_clip_slot clip";
    else log(path);
    this.path = path;
	this.liveObject = new LiveAPI(path);
    var name = this.liveObject.get("name");
    log("Success", name, path);
    //log("end_marker: ", this.liveObject.get("end_marker"));
    this.liveObject.set("end_marker", "8.00");
    this.liveObject.set("loop_end", "8.00");
}

Clip.prototype.getNotes = function(t0, p0, dt, dp) {
	if (!t0) t0 = 0;
	if (!p0) p0 = 0;
	if (!dt) dt = this.liveObject.get('length');
	if (!dp) dp = 128;
	var data = this.liveObject.call("get_notes", t0, p0, dt, dp);
	var notes = [];
	for (var i = 2, len = data.length - 1; i < len; i += 6) {
		var note = new Note(data[i+1], data[i+2], data[i+3], data[i+4], data[i+5]);
		notes.push(note);
	}
	return notes;
}

Clip.prototype.setNotes = function(notes) {
	var liveObject = this.liveObject;
	liveObject.call("set_notes");
	liveObject.call("notes", notes.length);
	notes.forEach (function(note) {
		liveObject.call("note", note.p, note.t0, note.dt, note.vel, note.muted);
	});
	liveObject.call("done");
}

// Note that fire and stop are linked to transport in the sense that they will start/stop at the sync time.
Clip.prototype.fire = function() {
    log("Launching: ", this.path);
	return this.liveObject.call("fire");
}

Clip.prototype.stop = function() {
    log("Stopping: ", this.path);
    return this.liveObject.call("stop");
}

Clip.prototype.get = function(s) {
    return this.liveObject.get(s);
}

Clip.prototype.clear = function() {
    log("Clearing: ", this.path);
    return this.liveObject.call("remove_notes", "0.0", 0, "8.0", 128);
}

exports.Clip = Clip;

/*
* Floats with quotes: Pitch, start time in beats, duration in beats.
* Int: Velocity, isMuted.
*/
function Note(p, t0, dt, vel, muted) {
	this.t0 = t0;
	this.p = p;
	this.dt = dt;
	this.vel = vel;
	this.muted = muted;
}

Note.prototype.toString = function() {
	return '{pitch: ' + this.p +
		', t0: ' + this.t0 +
		', dur: ' + this.dt +
		', vel: ' + this.vel + '}';
}

exports.Note = Note;
