var ret;

function bang() {
    outlet(0, ret);
}

var song;
function anything() {
    var args = arrayfromargs(messagename, arguments);
    // Warning: This will erase any existing arrangement recording on this track.
    song = new LiveAPI(live_set_observer, "live_set");
    song.property = "current_song_time";
    song.set("current_song_time", "0");
    song.set("is_playing", "0");
    ret = 0;
    bang();
    song.set("record_mode", "1");
    post("Sent init bang().\n");
}

var found1 = true;
var found2 = true;
var found3 = true;
var found4 = true;
var found = true;
var count = 1;
function live_set_observer(args) {
    var t = parseInt(args[1]);
    var dur = 32;
    if (count == 18) {
        post("Yayy! Stopping because t: ", t, " count: ", count, "\n");
        song.set("is_playing", "0");
        count = 0;
    }
    if (found && t > dur && t % dur == 1) {
        found = false;
        post("Yayy! Choosing preset ", count, " because t: ", t, "\n");
        ret = count;
        bang();
        count = count + 1;
        post("Sent bang() at t: " << t << "\n");
    } else {
        if (!found && t > dur && t % dur == 2) {
            found = true;
        }
    }
}
