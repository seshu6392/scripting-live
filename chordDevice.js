var common = require('common');
var assert = common.assert;

function ChordDevice(path) {
    if (!path) path = "live_set tracks 0 devices 1";
    else log(path);
    this.path = path;
    this.liveObject = new LiveAPI(path);
    var name = this.liveObject.get("name");
    assert (name == "Chord", name + " isn't a chord device");
    log("Success", name, path);
}

/* 
* knob is int (without quotes). 1 is shift1, 2 is shift2 etc.
* pitch is float with quotes. "12.00" shifts 1 octave above. "-12.00" below.
* velocity is float with quotes between "0.01" and "2.00". Note that this is relative to incoming MIDI note data.
*/ 
ChordDevice.prototype.pitchKnob = function(knob, pitch, velocity) {
    assert (knob >= 1 && knob <= 6);
    knob = 2 * (knob - 1) + 1;
    var pitchKnobObj = new LiveAPI(this.path + " parameters " + knob);
    var velKnob = knob + 1; 
    var velKnobObj = new LiveAPI(this.path + " parameters " + velKnob);
    if (!pitch) {
        var pitch = pitchKnobObj.get("value");
        var velocity = velKnobObj.get("value");
        //return "pitch: " + pitch + " velocity: " + velocity;
        return pitch;
    } 
    if (!velocity) velocity = "1.0"; 
    pitchKnobObj.set("value", pitch);
    velKnobObj.set("value", velocity); 
}

exports.ChordDevice = ChordDevice;
