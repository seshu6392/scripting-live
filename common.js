function assert(cond, msg) {
    if (!msg) msg = "Meh";
    if (!cond) throw log(msg); 
}

exports.assert = assert;

/*
* Thanks to http://compusition.com/writings/js-live-api-midi-clips-reading for intro to js+LiveAPI.
*/
function log() {
  for(var i=0,len=arguments.length; i<len; i++) {
    var message = arguments[i];
    if(message && message.toString) {
      var s = message.toString();
      if(s.indexOf("[object ") >= 0) {
        s = JSON.stringify(message);
      }
      post(s);
    }
    else if(message === null) {
      post("<null>");
    }
    else {
      post(message);
    }
  }
  post("\n");
}
 
//log("___________________________________________________");
//log("Reload:", new Date);

exports.log = log;
