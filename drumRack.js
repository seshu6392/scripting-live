var common = require('common');
var assert = common.assert;

function DrumRack(path) {
    if (!path) path = "live_set tracks 0 devices 1";
    else log(path);
    this.path = path;
    this.liveObject = new LiveAPI(path);
/*
    //log(this.liveObject.info);
    var numPads = this.liveObject.getcount("drum_pads");
    log(numPads);
    var padPath = this.path + " drum_pads " + 36;
    var pad = new LiveAPI(padPath);
    log(pad.info);
    log("name: ", pad.get("name"));
*/
    var name = this.liveObject.get("name");
    assert (name == "Drum Rack", name + " isn't a drum rack");
    log("Success", name, path);
    this.kicks = [36, 37, 38, 39];
    this.snares = [40, 41, 42, 43];
    this.toms = [44, 45, 46, 47];
    this.woods = [48, 49, 50, 51];
}

exports.DrumRack = DrumRack; 
