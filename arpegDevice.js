var common = require('common');
var assert = common.assert;

function ArpegDevice(path) {
    if (!path) path = "live_set tracks 0 devices 2";
    else log(path);
    this.path = path;
    this.liveObject = new LiveAPI(path);
    var name = this.liveObject.get("name");
    assert (name == "Arpeggiator", name + " isn't an arpeggiator device");
    log("Success", name, path);
    this.rateKnobObj = new LiveAPI(path + " parameters 5");
    this.gateKnobObj = new LiveAPI(path + " parameters 8");
    this.stepsKnobObj = new LiveAPI(path + " parameters 14");
}

/*
* rate is float with quotes in the range "1.0" to "13.0".
* From high to low: 1/1, 1/2, 1/3, 1/4, 1/6, 1/8, 1/12, 1/16, 1/24, 1/32, 1/64, 1/96, 1/128.
* Lazy to write an interpreter for this.
*/
ArpegDevice.prototype.rateKnob = function(rate) {
    if (!rate) return "rate: " + this.rateKnobObj.get("value");
    this.rateKnobObj.set("value", rate);  
}

/*
* gate is float with quotes in the range "1.0" to "200.0".
*/
ArpegDevice.prototype.gateKnob = function(gate) {
    if (!gate) return "gate: " + this.gateKnobObj.get("value");
    this.gateKnobObj.set("value", gate);  
}

/*
* steps is float with quotes in the range "0.0" to "8.0".
*/
ArpegDevice.prototype.stepsKnob = function(steps) {
    if (!steps) return "steps: " + this.stepsKnobObj.get("value");
    this.stepsKnobObj.set("value", steps);
}

exports.ArpegDevice = ArpegDevice;
