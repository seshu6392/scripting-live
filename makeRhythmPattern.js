var common = require('common');
var log = common.log;
var assert = common.assert;

var humanizingDeltaDur = 0.1;
var fillerDensity = 0.8;

var choices = [1, 2, 3, 4];
var target = 16;
// Goal: x1 + x2 + x3 + x4 + x5 + x6 = target with x1==x4 for familiarity.
// These are the onsents of strong-ish beats.
// Duration of first beat is (x2 - x1), sub-beats are marked by embellishments. 
// Will always start with the first beat, for simplicity.
var sum = 0;
for (var i = 0; i < choices.len; i++) {
    sum += choices[i];
    if (i != 0) assert (choices[i] >= choices[i -1]);
}
assert (sum <= target);

function populateRhythm() {
    var x1 = choices[Math.floor(Math.random() * (choices.length - 1) + 1)];
    var x2 = choices[Math.floor(Math.random() * (choices.length - 1) + 1)];
    if (x1 + x2 >= target) {
        x2 = choices[0];
    }
    assert (x1 + x2 < target);

    var x3 = choices[Math.floor(Math.random() * (choices.length - 1) + 1)];
    var x4 = x1;
    if (x1 + x2 + x3 + x4 >= target) {
        x3 = choices[0];
    }
    assert (x1 + x2 + x3 + x4 < target);

    var x5 = choices[Math.floor(Math.random() * (choices.length - 1) + 1)];
    if (x1 + x2 + x3 + x4 + x5 >= target) {
        x5 = choices[0];
    }
    x6 = target - (x1 + x2 + x3 + x4 + x5);
    assert (x6 > 0);

    log(x1, x2, x3, x4, x5, x6); 
    var ret = [x1, x2, x3, x4, x5, x6];
    return ret;
}

function rytToOnset(ryt) {
    var t = 0.0;
    var onsets = [t];
    var numBeats = 8;   // Hardcoded in Clip() for now.
    var beatDuration = numBeats * 1.0 / target;
    for (var i = 0; i < ryt.length - 1; i++) {
        t += (ryt[i] * beatDuration);
        onsets.push(t + Math.random() * humanizingDeltaDur - 0.5 * humanizingDeltaDur);
    } 
    log("onsets: ", onsets);
    assert (t < target);
    return onsets;
}

var clipReq = require('clip');
var Clip = clipReq.Clip;
var Note = clipReq.Note;

var drumReq = require('drumRack');
var DrumRack = drumReq.DrumRack;

//var clip1 = new Clip("live_set tracks 0 clip_slots 0 clip");
var clip1 = new Clip();
var drumRack = new DrumRack();
var kicks = drumRack.kicks;
var snares = drumRack.snares;
var toms = drumRack.toms;
var woods = drumRack.woods;

function makeSequence() {
    var ryt = populateRhythm();
    var onsets = rytToOnset(ryt);
    var notes = [];
    for (var i = 0; i < ryt.length; i++) {
        var key = kicks[Math.floor(Math.random() * kicks.length)];
        var t = onsets[i];
        var onset = onsets[i].toFixed(2);
        var dur = (8.0 - t).toFixed(2);
        if (i != ryt.length - 1) {
            dur = (onsets[i + 1] - onsets[i]).toFixed(2);
        }
        if (dur > 0.5 + humanizingDeltaDur) {
            var coinToss = Math.random();
            if (coinToss > (1 - fillerDensity)) {
                var fillerKey = woods[Math.floor(Math.random() * woods.length)];
                var fillerOnset = (onset * 1.0 + 0.5 + Math.random() * humanizingDeltaDur - 0.5 * humanizingDeltaDur).toFixed(2);
                var fillerDur = (dur - (fillerOnset - onset)).toFixed(2);
                var vel = (Math.random() * 70 + 20).toFixed(2);
                log("filler1:", fillerKey, fillerOnset, fillerDur, vel, 0);
                notes.push(new Note(fillerKey, fillerOnset, fillerDur, vel, 0));
            }
            if (dur > 1.0 + humanizingDeltaDur) {
                var coinToss = Math.random();
                if (coinToss > (1 - fillerDensity)) {
                    var fillerKey = toms[Math.floor(Math.random() * toms.length)];
                    var fillerOnset = (onset * 1.0 + 1.0 + Math.random() * humanizingDeltaDur - 0.5 * humanizingDeltaDur).toFixed(2);
                    var fillerDur = (dur - (fillerOnset - onset)).toFixed(2);
                    var vel = (Math.random() * 70 + 20).toFixed(2);
                    log("filler2:", fillerKey, fillerOnset, fillerDur, vel, 0);
                    notes.push(new Note(fillerKey, fillerOnset, fillerDur, vel, 0));
                }
                if (dur > 1.5 + humanizingDeltaDur) {
                    var coinToss = Math.random();
                    if (coinToss > (1 - fillerDensity)) {
                        var fillerKey = snares[Math.floor(Math.random() * snares.length)];
                        var fillerOnset = (onset * 1.0 + 1.5 + Math.random() * humanizingDeltaDur - 0.5 * humanizingDeltaDur).toFixed(2);
                        var fillerDur = (dur - (fillerOnset - onset)).toFixed(2);
                        var vel = (Math.random() * 60 + 20).toFixed(2);
                        log("filler3:", fillerKey, fillerOnset, fillerDur, vel, 0);
                        notes.push(new Note(fillerKey, fillerOnset, fillerDur, vel, 0));
                    }
                }
            }
        }
        var vel = (Math.random() * 70 + 20).toFixed(2);
        log(t, key, onset, dur, vel, 0);
        notes.push(new Note(key, onset, dur, vel, 0));
    }
    clip1.clear();
    clip1.setNotes(notes);
    //clip1.fire();
}

makeSequence();
